<?php
class History extends Controller
{
    public function __construct()
    {
        parent::__construct('history');
        $this->views->HistoryData = $this->model->GetAllHistoryData();

    }

    public function index()
    {
        $this->views->render('history/history');
    }

}
