<?php
class View
{
    public function __construct()
    {
        // echo '***************************<br/>';
        // echo 'Inside view.php Libs.<br/>';
        // echo '***************************<br/>';
    }

    public function render($name)
    {
        require_once 'views/' . $name . '.html';
        // if ($noInclude == true) {
        //     require_once 'views/' . $name . '.php';
        // } else {
        //     require_once 'views/header.php';
        //     require_once 'views/menu.php';
        //     require_once 'views/' . $name . '.php';
        //     require_once 'views/footer.php';
        // }
    }
}
