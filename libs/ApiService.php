<?php
class ApiService
{
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    //******** Index ********//

    //********************************************************************************//

    //List All exefileData
    public function GetAllData()
    {
        $this->loadModel("main");
        // // $postdata = file_get_contents("php://input");
        // // $request = json_decode($postdata);
        // // echo $request->mainID;
        // // $mID = $request->mainID;
        $this->model->GetAllData();
    }
}
