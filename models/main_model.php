<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //************** From Controllers **************//
    public function GetAllData()
    {
        $sql = 'SELECT *  FROM detector';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        $jsonData = json_encode($data);
        echo $jsonData;
    }
}
