<?php
class History_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAllHistoryData(){
        $sql = 'SELECT *  FROM detector';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);

        // print_r(count($data));
        $allWorkbench = [];
        $workbench = "";
        for ($i=0; $i < count($data); $i++) {
            $status = "";
            if ($data[$i]['status'] == "PASS") {
                $status = "<td class='badge badge-success'>".$data[$i]['status']."</td>";
            } else if ($data[$i]['status'] == "NOT") {
                $status = "<td class='badge badge-danger'>".$data[$i]['status']."</td>";
            }
            $workbench .= "<tr><th scope='row'>".$data[$i]['workbench']."</th><td>".$data[$i]['equipment']."</td><td>".$data[$i]['resistance']."</td>".$status."<td>".$data[$i]['time']."</td></tr>";
        }

        array_push($allWorkbench, array('workbench' => $workbench));
        // array_push($allWorkbench, $workbench1);

        // print_r($allWorkbench);
        return json_encode($allWorkbench);

        // foreach ($data as $key => $value) {
        //     if ($value["type"] == "1") {
        //         $workbench1 .= "<tr>
        //             <th scope='row'>".$value["value"]."</th>
        //             <td>Test</td>
        //             <td>Test</td>
        //             <td>Test</td>
        //         </tr>";
        //     }
        // }

        // <tr>
        //     <th scope="row">1</th>
        //     <td>Mark</td>
        //     <td>Otto</td>
        //     <td>@mdo</td>
        // </tr>

        // $jsonData = json_encode($data);
        // return $jsonData;
    }
}
